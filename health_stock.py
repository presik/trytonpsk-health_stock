from datetime import datetime

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateTransition
from trytond.pyson import If, Or, Eval, Not, Bool
from trytond.exceptions import UserError
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserWarning

_STATES = {
    'readonly': Eval('state') == 'done',
    }
_DEPENDS = ['state']


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'
    origin = fields.Reference('Origin', selection='get_origin', select=True,
        states={'readonly': Eval('state') != 'draft'}, depends=['state'])

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.appointment']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    warehouse = fields.Many2One('stock.location', 'Warehouse',
        domain=[('type', 'in', ['warehouse', 'storage'])],
        states={
            'invisible': Not(Bool(Eval('is_pharmacy'))),
            'required': Bool(Eval('is_pharmacy')),
            },
        depends=['is_pharmacy'])

    @classmethod
    def default_warehouse(cls):
        Location = Pool().get('stock.location')
        locations = Location.search(cls.warehouse.domain)
        if len(locations) == 1:
            return locations[0].id


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'
    expiration_date = fields.Date('Expiration Date')


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + [
            'health.prescription.order',
            'health.patient.ambulatory_care',
            'health.patient.rounding',
            'health.vaccination',
            'health.inpatient',
        ]


class PatientAmbulatoryCare(Workflow, ModelSQL, ModelView):
    'Patient Ambulatory Care'
    __name__ = 'health.patient.ambulatory_care'

    care_location = fields.Many2One('stock.location', 'Care Location',
        domain=[('type', '=', 'storage')],
        states={
            'required': If(Or(Bool(Eval('medicaments')),
                Bool(Eval('medical_supplies'))), True, False),
            'readonly': Eval('state') == 'done',
        }, depends=['state', 'medicaments'])
    medicaments = fields.One2Many(
        'health.patient.ambulatory_care.medicament', 'ambulatory_care',
        'Medicaments', states=_STATES, depends=_DEPENDS)
    medical_supplies = fields.One2Many(
        'health.patient.ambulatory_care.medical_supply', 'ambulatory_care',
        'Medical Supplies', states=_STATES, depends=_DEPENDS)
    moves = fields.One2Many('stock.move', 'origin', 'Stock Moves',
        readonly=True)

    @classmethod
    def __setup__(cls):
        super(PatientAmbulatoryCare, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
        ))
        cls._buttons.update({
            'done': {'invisible': ~Eval('state').in_(['draft'])}
        })

    @classmethod
    def copy(cls, ambulatory_cares, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['moves'] = None
        return super(PatientAmbulatoryCare, cls).copy(
            ambulatory_cares, default=default)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, ambulatory_cares):
        Professional = Pool().get('health.professional')

        lines_to_ship = {}
        medicaments_to_ship = []
        supplies_to_ship = []

        signing_hp = Professional.get_health_professional()

        for ambulatory in ambulatory_cares:
            for medicament in ambulatory.medicaments:
                medicaments_to_ship.append(medicament)

            for medical_supply in ambulatory.medical_supplies:
                supplies_to_ship.append(medical_supply)

        lines_to_ship['medicaments'] = medicaments_to_ship
        lines_to_ship['supplies'] = supplies_to_ship

        cls.create_stock_moves(ambulatory_cares, lines_to_ship)

        cls.write(ambulatory_cares, {
            'signed_by': signing_hp,
            'session_end': datetime.now()
        })

    @classmethod
    def create_stock_moves(cls, ambulatory_cares, lines):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        moves = []
        for ambulatory in ambulatory_cares:
            for round_medic in lines['medicaments']:
                move_info = {}
                move_info['origin'] = str(ambulatory)
                move_info['product'] = round_medic.medicament.product.id
                move_info['uom'] = round_medic.medicament.product.default_uom.id
                move_info['quantity'] = round_medic.quantity
                move_info['from_location'] = ambulatory.care_location.id
                move_info['to_location'] = \
                    ambulatory.patient.party.customer_location.id
                move_info['unit_price'] = \
                    round_medic.medicament.product.list_price
                if round_medic.lot:
                    if round_medic.lot.expiration_date \
                    and round_medic.lot.expiration_date < Date.today():
                        raise UserError('Expired medicaments')
                    move_info['lot'] = round_medic.lot.id
                moves.append(move_info)

            for medical_supply in lines['supplies']:
                move_info = {}
                move_info['origin'] = str(ambulatory)
                move_info['product'] = medical_supply.product.id
                move_info['uom'] = medical_supply.product.default_uom.id
                move_info['quantity'] = medical_supply.quantity
                move_info['from_location'] = ambulatory.care_location.id
                move_info['to_location'] = \
                    ambulatory.patient.party.customer_location.id
                move_info['unit_price'] = medical_supply.product.list_price
                if medical_supply.lot:
                    if medical_supply.lot.expiration_date \
                    and medical_supply.lot.expiration_date < Date.today():
                        raise UserError('Expired supplies')
                    move_info['lot'] = medical_supply.lot.id
                moves.append(move_info)

        new_moves = Move.create(moves)
        Move.write(new_moves, {
            'state': 'done',
            'effective_date': Date.today(),
        })
        return True


class PatientAmbulatoryCareMedicament(ModelSQL, ModelView):
    'Patient Ambulatory Care Medicament'
    __name__ = 'health.patient.ambulatory_care.medicament'

    ambulatory_care = fields.Many2One('health.patient.ambulatory_care',
        'Ambulatory ID')
    medicament = fields.Many2One('health.medicament', 'Medicament',
        required=True)
    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char('Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One('stock.lot', 'Lot', depends=['product'],
        domain=[('product', '=', Eval('product'))])

    @staticmethod
    def default_quantity():
        return 1

    @fields.depends('medicament')
    def on_change_medicament(self):
        if self.medicament:
            self.product = self.medicament.product.id
        else:
            self.product = None


class PatientAmbulatoryCareMedicalSupply(ModelSQL, ModelView):
    'Patient Ambulatory Care Medical Supply'
    __name__ = 'health.patient.ambulatory_care.medical_supply'

    ambulatory_care = fields.Many2One('health.patient.ambulatory_care',
        'Ambulatory ID')
    product = fields.Many2One('product.product', 'Medical Supply',
        domain=[('is_medical_supply', '=', True)], required=True)
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char('Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One('stock.lot', 'Lot', depends=['product'],
        domain=[
            ('product', '=', Eval('product')),
        ])

    @staticmethod
    def default_quantity():
        return 1

# Moved to Inpatient
# class Inpatient(metaclass=PoolMeta):
#     'Inpatient'
#     __name__ = 'health.inpatient'
#     hospitalization_location = fields.Many2One('stock.location',
#         'Hospitalization Location', domain=[('type', '=', 'warehouse')],
#         states={
#             'required': Bool(Eval('medications')),
#             'readonly': Eval('state') == 'finished',
#         }, depends=_DEPENDS)
#     shipments = fields.Function(fields.One2Many('stock.shipment.out', None,
#         'Shipments'), 'get_shipments')
#
#     def get_shipments(self, name=None):
#         Move = Pool().get('stock.move')
#         moves = Move.search([
#             ('origin', '=', str(self)),
#         ])
#         return [m.shipment.id for m in moves if m.shipment]
#
#     def generate_daily_shipment(self):
#         pool = Pool()
#         Shipment = pool.get('stock.shipment.out')
#         Date = pool.get('ir.date')
#         to_create = []
#         moves = self.get_stock_moves(self.medications)
#         to_create.append({
#             'planned_date': Date.today(),
#             'company': Transaction().context.get('company'),
#             'customer': self.patient.party.id,
#             'reference': self.code,
#             'warehouse': self.hospitalization_location.id,
#             'delivery_address': self.patient.party.addresses[0].id,
#             'moves': [('add', moves)],
#         })
#         if to_create:
#             Shipment.create(to_create)
#
#     def get_stock_moves(self, medications):
#         pool = Pool()
#         Move = pool.get('stock.move')
#         moves = []
#         for medication in medications:
#             move_info = {}
#             move_info['origin'] = str(self)
#             move_info['product'] = medication.medicament.product.id
#             move_info['uom'] = medication.medicament.product.default_uom.id
#             move_info['quantity'] = medication.qty
#             move_info['from_location'] = self.hospitalization_location.output_location.id
#             move_info['to_location'] = self.patient.party.customer_location.id
#             move_info['unit_price'] = medication.medicament.product.list_price
#             move_info['state'] = 'draft'
#             moves.append(move_info)
#         new_moves = Move.create(moves)
#         return new_moves


class PatientRounding(Workflow, ModelSQL, ModelView):
    'Patient Ambulatory Care'
    __name__ = 'health.patient.rounding'

    hospitalization_location = fields.Many2One('stock.location',
        'Hospitalization Location', domain=[('type', '=', 'storage')],
        states={
            'required': If(Or(Bool(Eval('medicaments')),
                Bool(Eval('medical_supplies'))), True, False),
            'readonly': Eval('state') == 'done',
        }, depends=_DEPENDS)
    medicaments = fields.One2Many('health.patient.rounding.medicament',
        'rounding', 'Medicaments', states=_STATES, depends=_DEPENDS)
    medical_supplies = fields.One2Many(
        'health.patient.rounding.medical_supply', 'rounding',
        'Medical Supplies', states=_STATES, depends=_DEPENDS)
    moves = fields.One2Many('stock.move', 'origin', 'Stock Moves',
        readonly=True)

    @classmethod
    def __setup__(cls):
        super(PatientRounding, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
        ))
        cls._buttons.update({
            'done': {'invisible': ~Eval('state').in_(['draft'])}
        })

    @classmethod
    def copy(cls, roundings, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['moves'] = None
        return super(PatientRounding, cls).copy(roundings, default=default)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, roundings):
        sign_prof = Pool().get('health.professional').get_health_professional()

        lines_to_ship = {}
        medicaments_to_ship = []
        supplies_to_ship = []

        for rounding in roundings:
            for medicament in rounding.medicaments:
                medicaments_to_ship.append(medicament)

            for medical_supply in rounding.medical_supplies:
                supplies_to_ship.append(medical_supply)

        lines_to_ship['medicaments'] = medicaments_to_ship
        lines_to_ship['supplies'] = supplies_to_ship

        cls.create_stock_moves(roundings, lines_to_ship)
        cls.write(roundings, {
            'signed_by': sign_prof,
            'evaluation_end': datetime.now()
        })

    @classmethod
    def create_stock_moves(cls, roundings, lines):
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        moves = []
        for rounding in roundings:
            for round_medic in lines['medicaments']:
                move_info = {}
                move_info['origin'] = str(rounding)
                move_info['product'] = round_medic.medicament.product.id
                move_info['uom'] = round_medic.medicament.product.default_uom.id
                move_info['quantity'] = round_medic.quantity
                move_info['from_location'] = \
                    rounding.hospitalization_location.id
                move_info['to_location'] = \
                rounding.name.patient.name.customer_location.id
                move_info['unit_price'] = round_medic.product.template.list_price
                if round_medic.lot:
                    if round_medic.lot.expiration_date \
                    and round_medic.lot.expiration_date < Date.today():
                        raise UserError('Expired medicaments')
                    move_info['lot'] = round_medic.lot.id
                moves.append(move_info)
            for medical_supply in lines['supplies']:
                move_info = {}
                move_info['origin'] = str(rounding)
                move_info['product'] = medical_supply.product.id
                move_info['uom'] = medical_supply.product.default_uom.id
                move_info['quantity'] = medical_supply.quantity
                move_info['from_location'] = \
                    rounding.hospitalization_location.id
                move_info['to_location'] = \
                    rounding.name.patient.name.customer_location.id
                move_info['unit_price'] = medical_supply.product.list_price
                if medical_supply.lot:
                    if medical_supply.lot.expiration_date \
                    and medical_supply.lot.expiration_date < Date.today():
                        raise UserError('Expired supplies')
                    move_info['lot'] = medical_supply.lot.id
                moves.append(move_info)
        new_moves = Move.create(moves)
        Move.write(new_moves, {
            'state': 'done',
            'effective_date': Date.today(),
        })
        return True


class PatientRoundingMedicament(ModelSQL, ModelView):
    'Patient Rounding Medicament'
    __name__ = 'health.patient.rounding.medicament'

    rounding = fields.Many2One('health.patient.rounding', 'Ambulatory ID')
    medicament = fields.Many2One('health.medicament', 'Medicament',
        required=True)
    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char('Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One('stock.lot', 'Lot', depends=['product'],
        domain=[('product', '=', Eval('product'))])

    @staticmethod
    def default_quantity():
        return 1

    @fields.depends('medicament')
    def on_change_medicament(self):
        if self.medicament:
            self.product = self.medicament.product.id
        else:
            self.product = None


class PatientRoundingMedicalSupply(ModelSQL, ModelView):
    'Patient Rounding Medical Supply'
    __name__ = 'health.patient.rounding.medical_supply'

    rounding = fields.Many2One('health.patient.rounding', 'Ambulatory ID')
    product = fields.Many2One('product.product', 'Medical Supply',
        domain=[('is_medical_supply', '=', True)], required=True)
    quantity = fields.Integer('Quantity')
    short_comment = fields.Char('Comment',
        help='Short comment on the specific drug')
    lot = fields.Many2One('stock.lot', 'Lot', depends=['product'],
        domain=[
            ('product', '=', Eval('product')),
        ])

    @staticmethod
    def default_quantity():
        return 1


class PatientPrescriptionOrder(metaclass=PoolMeta):
    __name__ = 'health.prescription.order'
    moves = fields.One2Many('stock.move', 'origin', 'Moves', readonly=True)

    @classmethod
    def __setup__(cls):
        super(PatientPrescriptionOrder, cls).__setup__()
        cls.pharmacy.states['readonly'] &= Bool(Eval('moves'))
        cls.pharmacy.depends.append('moves')

    @classmethod
    def copy(cls, prescriptions, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['moves'] = None
        return super(PatientPrescriptionOrder, cls).copy(
            prescriptions, default=default)


class PatientVaccination(metaclass=PoolMeta):
    __name__ = 'health.vaccination'
    moves = fields.One2Many('stock.move', 'origin', 'Moves', readonly=True)
    location = fields.Many2One('stock.location',
        'Stock Location', domain=[('type', '=', 'storage')])
    product = fields.Many2One('product.product', 'Product')
    lot = fields.Many2One('stock.lot', 'Lot', depends=['product'],
        domain=[('product', '=', Eval('product'))],
        help="This field includes the lot number and expiration date")

    @fields.depends('lot')
    def on_change_lot(self):
        # Check expiration date on the vaccine lot
        if self.lot:
            if self.lot.expiration_date < datetime.date(self.date):
                raise UserWarning(gettext('health_stock.msg_expired_vaccine'))
        return {}

    @classmethod
    def copy(cls, vaccinations, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['moves'] = None
        return super(PatientVaccination, cls).copy(
            vaccinations, default=default)

    @classmethod
    def __setup__(cls):
        super(PatientVaccination, cls).__setup__()

    @fields.depends('vaccine')
    def on_change_vaccine(self):
        if self.vaccine:
            self.product = self.vaccine.product.id
        else:
            self.product = None


class CreateInpatientShipment(Wizard):
    """
    Create Inpatient Shipment
    """
    __name__ = 'health.inpatient.create_shipment'
    start_state = 'generate_shipment'
    generate_shipment = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateInpatientShipment, cls).__setup__()

    def transition_generate_shipment(self):
        pool = Pool()
        Inpatient = pool.get('health.inpatient')
        ids = Transaction().context['active_ids']
        if ids:
            for inpatient in Inpatient.browse(ids):
                if inpatient.state in ('hospitalized', 'confirmed'):
                    inpatient.generate_daily_shipment()
        return 'end'


class CreateStockShipment(Wizard):
    """
    Create Stock Shipment
    """
    __name__ = 'health.stock.create_shipment'
    start_state = 'generate_shipment'
    generate_shipment = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateStockShipment, cls).__setup__()

    def transition_generate_shipment(self):
        ids = Transaction().context['active_ids']
        model = Transaction().context['active_model']
        print('Transaction().context ...', Transaction().context)
        HealthModel = Pool().get(model)
        if ids:
            for record in HealthModel.browse(ids):
                record.generate_shipment()
        return 'end'
