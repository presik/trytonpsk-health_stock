from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.model import ModelView
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.i18n import gettext
from trytond.exceptions import UserError


class CreateVaccinationStockMoveInit(ModelView):
    'Create Vaccination Stock Move Init'
    __name__ = 'health.vaccination.stock.move.init'


class CreateVaccinationStockMove(Wizard):
    'Create Vaccination Stock Move'
    __name__ = 'health.vaccination.stock.move.create'

    start = StateView('health.vaccination.stock.move.init',
        'health_stock.view_create_vaccination_stock_move', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Stock Move', 'create_stock_move', 'tryton-ok', True),
        ])
    create_stock_move = StateTransition()

    def transition_create_stock_move(self):
        pool = Pool()
        StockMove = pool.get('stock.move')
        Vaccination = pool.get('health.vaccination')

        vaccinations = Vaccination.browse(Transaction().context.get(
            'active_ids'))
        for vaccination in vaccinations:
            if vaccination.moves:
                raise UserError(gettext('health_stock.msg_move_exists'))

            lines = []
            line_data = {}
            line_data['origin'] = str(vaccination)
            line_data['from_location'] = \
                vaccination.location.id
            line_data['to_location'] = \
                vaccination.name.name.customer_location.id
            line_data['product'] = \
                vaccination.vaccine.name.id
            line_data['unit_price'] = \
                vaccination.vaccine.name.list_price
            line_data['quantity'] = 1
            line_data['uom'] = \
                vaccination.vaccine.name.default_uom.id
            line_data['state'] = 'draft'
            lines.append(line_data)

            moves = StockMove.create(lines)
            StockMove.assign(moves)
            StockMove.do(moves)
        return 'end'
